# File System Manager

Java console application that can be used to manage and manipulate files.

The program contains:
* User interaction and input
* File manipulation
* Logging

## Functionality

Users can print all files in resources folder and find files by extension.
Additionally, users can check the included .txt file's details (filename, size and total number of lines). It is also possible to search said .txt file for words, the search result will print the number of occurences of that word in the file.
Each method responsible for file manipulation triggers a new entry in the log file. Log file entries have the timestamp for which they were generated, as well as, information on which service triggered the log entry and execution time in ms.

## Output

Steps taken to compile application into a single .jar:

![](src/demo/demo.gif)