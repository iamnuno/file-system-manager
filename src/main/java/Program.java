package main.java;

import java.util.Scanner;

import main.java.menus.Menu;

/**
 * Entry point for application.
 */
public class Program {
    public static void main(String[] args) {
        Program myProgram = new Program();
        myProgram.start();
    }

    private void start() {
        try (Scanner input = new Scanner(System.in)) {
            Menu.displayMainMenu(input);
        }
    }
}