package main.java.menus;

import java.time.LocalDateTime;
import java.util.Scanner;

import main.java.services.FileService;
import main.java.services.LogService;

/**
 * Responsible for display user menus.
 * User selection can trigger new menus or calls to other services.
 * Calls to services are logged.
 */
public class Menu {
    
    public static void displayMainMenu(Scanner input) {
        String option;
        do {
            System.out.println();
            System.out.println("### File System Manager ###");
            System.out.println("");
            System.out.println("[1] List all files");
            System.out.println("[2] Find files by extension");
            System.out.println("[3] Manipulation .txt");
            System.out.println("[Q] Quit");
            System.out.println("");
            System.out.print("Enter option > ");

            option = input.nextLine().toLowerCase().trim();
            switch (option) {
                case "1":
                    long start = System.nanoTime();
                    FileService.listResources();
                    long end = System.nanoTime();
                    LogService.log("List all files", start, end, LocalDateTime.now());
                    break;
                case "2":
                    displayFindFilesByExtensionMenu(input);
                    break;
                case "3":
                    displayManipulationTxtMenu(input);
                    break;
                case "q":
                    System.out.println("Bye!");
                    break;
                default:
                    System.out.println("Invalid option.");
            }
        } while (!option.equals("q"));
    }

    public static void displayFindFilesByExtensionMenu(Scanner input) {
        String option;
        do {
            System.out.println("");
            System.out.println("Find files by extension:");
            System.out.println("");
            System.out.println("[1] Choose extension");
            System.out.println("[Q] Back to main menu");
            System.out.println("");
            System.out.print("Enter option > ");

            option = input.nextLine().toLowerCase().trim();
            switch (option) {
                case "1":
                    System.out.println("");
                    System.out.print("Enter extension > ");
                    String extension = input.nextLine().toLowerCase().trim();
                    long start = System.nanoTime();
                    FileService.listResources(extension);
                    long end = System.nanoTime();
                    LogService.log(String.format("List files with extension '%s'", extension), start, end, LocalDateTime.now());
                    break;
                case "q":
                    break;
                default:
                    System.out.println("Invalid option.");
            }
        } while (!option.equals("q"));
    }

    public static void displayManipulationTxtMenu(Scanner input) {
        String option;
        do {
            System.out.println("");
            System.out.println("Manipulation .txt:");
            System.out.println("");
            System.out.println("[1] Show file details");
            System.out.println("[2] Search for word");
            System.out.println("[Q] Back to main menu");
            System.out.println("");
            System.out.print("Enter option > ");

            option = input.nextLine().toLowerCase().trim();
            switch (option) {
                case "1":
                    long start = System.nanoTime();
                    FileService.displayTxtFileInfo();
                    long end = System.nanoTime();
                    LogService.log("Show .txt file details", start, end, LocalDateTime.now());
                    break;
                case "2":
                    System.out.println("");
                    System.out.print("Enter word > ");
                    String search = input.nextLine().toLowerCase().trim();
                    start = System.nanoTime();
                    FileService.searchWordTxtFile(search);
                    end = System.nanoTime();
                    LogService.log(String.format("Search .txt file for '%s'", search), start, end, LocalDateTime.now());
                    break;
                case "q":
                    break;
                default:
                    System.out.println("Invalid option.");
            }
        } while (!option.equals("q"));
    }
}
