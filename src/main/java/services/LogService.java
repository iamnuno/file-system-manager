package main.java.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

/**
 * Logs execution time to a log file.
 */
public class LogService {
    
    /**
     * Calculates execution time in miliseconds, given start and end time and logs it to file.
     * Start and end time should be passed as 'System.nanoTime()' longs for better accuracy.
     * Custom string message allows to keep track of which service was executed.
     * Path is built based on .jar execution from 'out/' folder and
     * log file in 'src/logs/log.txt'.
     * 
     * @param message text to be logged
     * @param start   start time when method started its execution
     * @param end     end time when method started its execution
     * @param date    timestamp for when the entry is logged
     */
    public static void log(String message, long start, long end, LocalDateTime date) {
        long executionTime = (end - start) / 1000000;
        String logMessage = String.format("%s: %s took %s ms to execute.%n", date, message, executionTime);
        try {
            Path logfile = Paths.get(System.getProperty("user.dir"), "..", "src", "logs", "log.txt");
            Files.write(logfile, logMessage.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.out.println("\nCould not log operation");
        }
    }
}
