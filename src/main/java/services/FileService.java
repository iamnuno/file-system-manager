package main.java.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Responsible for handling file manipulation.
 */
public class FileService {

    /**
     * Returns resources directory where files are located for manipulation.
     * Path is built based on .jar execution from out/ folder and
     * resources folder in src/ folder.
     * 
     * @return the resources directory
     * @throws IOException
     */
    private static DirectoryStream<Path> loadResources() throws IOException {
        Path dir = Paths.get(System.getProperty("user.dir"), "..", "src", "resources");
        DirectoryStream<Path> stream = Files.newDirectoryStream(dir);
        return stream;
    }

    /**
     * Prints all files' names in resources directory.
     */
    public static void listResources() {
        try (DirectoryStream<Path> stream = loadResources()) {
            System.out.println("\nFiles found:");
            for (Path file : stream) {
                System.out.println(file.getFileName());
            }
        } catch (IOException e) {
            System.out.println("\nCould not get resources");
        } catch (DirectoryIteratorException e) {
            System.out.println("\nCould not iterate over directory");
        }
    }
    
    /**
     * Overloaded method to print files' names based on extension.
     * 
     * @param extension the file extension to be searched for
     */
    public static void listResources(String extension) {
        try (DirectoryStream<Path> stream = loadResources()) {
            System.out.println("\nFiles found:");
            boolean found = false;
            for (Path file : stream) {
                String filename = file.getFileName().toString();
                int separator = filename.indexOf(".");
                String str = filename.substring(separator + 1);
                if (str.equals(extension)) {
                    System.out.println(filename);
                    found = true;
                }
            }
            if (!found) {
                System.out.printf("No file(s) found with extension '%s'.%n", extension);
            }
        } catch (IOException e) {
            System.out.println("\nCould not get resources");
        } catch (DirectoryIteratorException e) {
            System.out.println("\nCould not iterate over directory");
        }
    }

    /**
     * Displays .txt file info in resources: filename, size and line count.
     */
    public static void displayTxtFileInfo() {
        try (DirectoryStream<Path> stream = loadResources()) {
            for (Path file : stream) {
                String filename = file.getFileName().toString();
                int separator = filename.indexOf(".");
                String str = filename.substring(separator);
                if (str.equals(".txt")) {
                    System.out.printf("%nFile name: %s%n", filename);
                    System.out.printf("File size (in bytes): %s%n", Files.size(file));
                    System.out.printf("File total lines: %s%n", countLines(file.toString()));
                }
            }
        } catch (IOException e) {
            System.out.println("\nCould not get resources");
        } catch (DirectoryIteratorException e) {
            System.out.println("\nCould not iterate over directory");
        }
    }

    /**
     * Searches the .txt file for a specific word.
     * File is read line by line and prints the
     * number of occurences for said word.
     * 
     * @param word the string to be searched for in the .txt file
     */
    public static void searchWordTxtFile(String word) {
        try (DirectoryStream<Path> stream = loadResources()) {
            for (Path file : stream) {
                String filename = file.getFileName().toString();
                int separator = filename.indexOf(".");
                String str = filename.substring(separator);
                if (str.equals(".txt")) {
                    try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file.toString()))) {
                        int count = 0;
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            // Words are split on non-alpha numeric characters
                            // Needed for occurences such as 'sleep----as'
                            String[] words = line.split("\\W");
                            for (String w : words) {
                                if (w.equalsIgnoreCase(word)) {
                                    count++;
                                }
                            }
                        }
                        System.out.printf("%nWord '%s' occurences: %s%n", word, count);
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("\nCould not get resources");
        } catch (DirectoryIteratorException e) {
            System.out.println("\nCould not iterate over directory");
        }
    }

    /**
     * Helper method to count lines in a file, based on the path
     * given to the file.
     * 
     * @param pathToFile the path to the file
     */
    private static int countLines(String pathToFile) {
        int lines = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
            while ((bufferedReader.readLine()) != null) {
                lines++;
            }
        } catch (IOException ex) {
            System.out.println("\nCould not read .txt file");
        }
        return lines;
    }
}
